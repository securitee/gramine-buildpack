# Node.js Sample App using Yarn

## Building

```
export BP_SGX_SIGNING_KEY=$(base64 -w0 path/to/signing-key.pem)
pack build --clear-cache \
           --buildpack gcr.io/paketo-buildpacks/nodejs \
           --buildpack paketo-buildpacks/cpython \
           --buildpack paketo-buildpacks/pip \
           --buildpack registry.gitlab.com/securitee/gramine-buildpack \
           --env BP_SGX_SIGNING_KEY \
           --env PYTHONPYCACHEPREFIX=/tmp/bytecode \
           yarn-sample
```

## Running

`docker run --interactive --tty --publish 8080:8080 --device /dev/sgx_enclave yarn-sample`

## Viewing

`curl http://localhost:8080`
