# Intro
The goal of the Gramine Buildpack is to decrease the complexity of building Intel SGX applications and therefore lower the barrier to entry for developers who are not familiar with Intel SGX technology. It does so by including the [Gramine libOS](https://github.com/gramineproject/gramine) - a library OS designed to run unmodified code in enclaves - in a [cloud native buildpack](https://buildpacks.io/). In conjunction with other source specific buildpacks it provides a straight forward process for converting source code to a containerized Intel SGX enclave.

In addition to its low complexity - due to the way buildpacks are designed - the Gramine Buildpack comes with deterministic builds at no additional costs. It makes it a very useful tool in Trustless Computing environments, where any person needs to be able to audit and verify the integrity of a Confidential Computing application using only the source code and attestation report.

# Configure
The Gramine Buildpack will activate if it detects a file `app.manifest` in the project root folder. This file accepts gramine manifest related configuration as [defined in the gramine docs](https://gramine.readthedocs.io/en/latest/manifest-syntax.html). Similar to [GSC](https://gramine.readthedocs.io/projects/gsc/en/latest/) most of the low level gramine manifest configuration is already [handled by the buildpack](entrypoint.manifest.template). For example, the whole container file system is mounted within the enclave and files within the directories `/layers` and `/workspace` are preconfigured as trusted files. Symlinks in these folders are not included and must be added manually. Process entrypoint and argument loading is also taken care of by the enclave. Common configuration that a user might still want to supply via the `app.manifest` file might be:
* additional allowed, trusted and protected files
* environment variables
* hardware requirements such as number of threads and enclave memory size

## Custom command and arguments
Users may want to override buildpack-provided processes or augment the app-image with additional process types using the `Procfile` [buildpack](https://paketo.io/docs/howto/configuration/#procfiles). For each additional process a user defines via the `Procfile`, the gramine buildpack will create and sign a seperate manifest file and hence create a separate enclave.

## Signing key
For the buildpack to be able to sign the Intel SGX enclave, a key needs to be provided. The signing key is passed via the environment variable `BP_SGX_SIGNING_KEY`. It holds a base64 encoded string representing the PEM formatted key file. 
```
export BP_SGX_SIGNING_KEY=$(base64 -w0 path/to/signing-key.pem)
```

> **Warning:** the signing key will be passed to the build lifecycle and with that, all buildpacks participating in the build will be able to read it. Verify beforehand whether this could be an issue.

# Build
```
pack build --clear-cache \
           ... \
           --buildpack paketo-buildpacks/cpython \
           --buildpack paketo-buildpacks/pip \ 
           --buildpack registry.gitlab.com/securitee/gramine-buildpack \
           --builder paketobuildpacks/builder:base \
           --env BP_SGX_SIGNING_KEY \
           --env PYTHONPYCACHEPREFIX=/tmp/bytecode \
           sample-sgx-application
```

 * `--clear-cache` - the Gramine Buildpack currently [does not support caching](#build-caching) of previous buildpack layers.
 * `BP_SGX_SIGNING_KEY` - references the enclave [signing key](#signing-key)
 * `PYTHONPYCACHEPREFIX` - required for [deterministic builds](#deterministic-builds)
 * `cpython` and `pip` buildpacks - required dependencies of the Gramine Buildpack itself.

# Run
Starting the container will run the graminized application as the default process.
```
docker run -it --device /dev/sgx_enclave sample-sgx-application
```

Run `pack inspect` to find different process options.
```
$ pack inspect sample-sgx-application
Inspecting image: sample-sgx-application

...

Processes:
  TYPE                         SHELL        COMMAND                                                              ARGS
  web-gramine (default)        bash         /layers/tech.securitee.buildpacks.gramine/sign/web/apploader         
  bash-gramine                 bash         /layers/tech.securitee.buildpacks.gramine/sign/bash/apploader        
  web                          bash         /bin/echo hello world
```

The Gramine Buildpack has converted the default startup process - in this example `web` - to an application capable of running inside an Intel SGX enclave. This startup process called `web-gramine` is the default entrypoint. The `web` entrypoint is the previous startup process and it will execute the application without gramine. `bash-gramine` will drop the user in a bash shell within the Intel SGX enclave. This may be useful for debugging. The different processes can be executed by setting them as container entrypoints via the `--entrypoint` parameter.

```
docker run --device /dev/sgx_enclave --entrypoint bash-gramine sample-sgx-application
```

## Execute with Linux PAL instead of Linux-SGX PAL
If a process is run that is configured to execute a gramine enclave, a user may select the Linux PAL at Docker run time instead of the Linux-SGX PAL by specifying the environment variable `GSC_PAL` as an option to the docker run command. For more details see [GSCs section](https://gramine.readthedocs.io/projects/gsc/en/latest/#execute-with-linux-pal-instead-of-linux-sgx-pal) on the same topic


# Deterministic builds
Buildpacks [aim to create “Reproducible Builds”](https://buildpacks.io/docs/features/reproducibility/) of container images. The gramine buildpack supports this feature given a few prerequisites. With deterministic builds it is possible to come to the exact same container image and hence the exact same `MRENCLAVE`. This is particularly useful to provide a third party the means to verify the contents of an enclave.

## Prerequesits
* deterministic build support by other buildpacks/their programming languages
* Pythons `.pycache` files include timestamps and hence are non deterministic. Store these files during build in a temporary directory via environment variable `PYTHONPYCACHEPREFIX=/tmp/bytecode`. 

## Verify determinism
Run the image build step twice, but change the name to <image-name>*-duplicate* for the second build. With these images run a diff on all buildpack layer hashes, if the images lifecycle metadata only differs in timestamps the build is deterministic. 
```
diff \
     <(docker inspect -f '{{ index .Config.Labels "io.buildpacks.lifecycle.metadata" }}' sample-sgx-application | jq .) \
     <(docker inspect -f '{{ index .Config.Labels "io.buildpacks.lifecycle.metadata" }}' sample-sgx-application-duplicate | jq .)
```

# Limitations
Due to gramines file integrity requirements a user might encounter issues with other buildpacks. It is important to understand these issues and be aware of the limitations that arise from them.

## Buildpack runtime scripts
Per the buildpack spec, a buildpack can specify scripts and executables to be run during runtime, before the main application is executed. These routines might modify files that have already been hashed and added to the signed manifest as trusted files. If the graminized application then accesses these modified files, it will fail, as the hash of this file will not match. Currently there is no real workaround except of preventing this situation happening.

## Build caching
During the build phase the gramine buildpack will expand all files found in the `/layers` directory and add them to the manifest as trusted files. Buildpacks supporting build caching might not restore their layer state if it is cached and therefore the trusted files expansion of the `/layers` directory will be incomplete, as the files of theses cached layers will not be added. To circumvent this, the `--clear-cache` flag can be added during the build with the `pack` CLI.
