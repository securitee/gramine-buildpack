use std::path::Path;
use std::process::Command;
use std::str;

use crate::GramineBuildpack;
use libcnb::build::BuildContext;
use libcnb::data::layer_content_metadata::LayerTypes;
use libcnb::generic::GenericMetadata;
use libcnb::layer::{Layer, LayerResult, LayerResultBuilder};
use libcnb::layer_env::{LayerEnv, ModificationBehavior, TargetLifecycle};

pub struct PipLayer;

impl Layer for PipLayer {
    type Buildpack = GramineBuildpack;
    type Metadata = GenericMetadata;

    fn types(&self) -> LayerTypes {
        LayerTypes {
            build: true,
            launch: true,
            cache: true,
        }
    }

    fn create(
        &self,
        _context: &BuildContext<Self::Buildpack>,
        layer_path: &Path,
    ) -> anyhow::Result<LayerResult<Self::Metadata>> {
        println!("---> Installing python dependencies");
        //let cache_dir = tempdir()?;
        
        let pip_exit_code = Command::new("pip")
            .args(&[
                "install",
                "-Iv",
                "click==8.0.3",
                "jinja2==3.0.3",
                "protobuf==3.19.3",
                "toml==0.10.2",
                "MarkupSafe==2.0.1",
                "--exists-action=w",
                //format!("--cache-dir={}", cache_dir.path().display()).as_ref(),
                "--compile",
				"--user",
				"--disable-pip-version-check",
            ])
            .env("PYTHONUSERBASE",layer_path.to_str().unwrap())
            .spawn()?
            .wait()?;

        if !pip_exit_code.success() {
            return Err(anyhow::anyhow!("Could not pip install"));
        }
 
        let python_result = Command::new("python")
            .args(&[
                "-m",
                "site",
                "--user-site",
            ])
            .env("PYTHONUSERBASE",layer_path.to_str().unwrap())
            .output()?;

        if !python_result.status.success() {
            return Err(anyhow::anyhow!("Failed to locate site packages"));
        }

        let path: String = str::from_utf8(&python_result.stdout)?.split_whitespace().collect();

        if path.is_empty() {
            return Err(anyhow::anyhow!("Failed to locate site packages: output is empty"));
        }

        LayerResultBuilder::new(GenericMetadata::default())
            .env(
                LayerEnv::new()
                    .chainable_insert(
                        TargetLifecycle::All,
                        ModificationBehavior::Prepend,
                        "PYTHONPATH",
                        format!("{}:",path),
                    )
                    .chainable_insert(
                        TargetLifecycle::All,
                        ModificationBehavior::Delimiter,
                        "PYTHONPATH",
                        ":",
                    )
            )
            .build()

    }
}
