use glob::glob;
use serde::Deserialize;
use serde::Serialize;
use std::collections::HashMap;
use std::env;
use std::fs;
use std::fs::OpenOptions;
use std::io::Write;
use std::path::Path;
use std::process::Command;
use std::str;
use tempfile::NamedTempFile;

use crate::GramineBuildpack;
use libcnb::build::BuildContext;
use libcnb::data::layer_content_metadata::LayerTypes;
use libcnb::data::launch::{Launch, Process};
use libcnb::data::process_type;
use libcnb::layer::{Layer, LayerResult, LayerResultBuilder};
use libcnb::layer_env::{LayerEnv, ModificationBehavior, TargetLifecycle};
use libcnb::Env;

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct SignLayerMetadata {
    pub processes: Vec<Process>,
}

pub struct SignLayer{
    pub gramine_layer_path: String,
    pub gramine_env: Env,
}

trait UpdatableProcess {
    fn update(&mut self, process: Process);
}

impl UpdatableProcess for Process {
    fn update(&mut self, process: Process) {
        self.r#type = process.r#type;
        self.command = process.command;
        if process.args.is_some() {
            self.args = process.args;
        }
        if process.direct.is_some() {
            self.direct = process.direct;
        }
        if process.default.is_some() {
            self.default = process.default;
        }
    }
}

impl Layer for SignLayer {
    type Buildpack = GramineBuildpack;
    type Metadata = SignLayerMetadata;

    fn types(&self) -> LayerTypes {
        LayerTypes {
            build: true,
            launch: true,
            cache: true,
        }
    }

    fn create(
        &self,
        context: &BuildContext<Self::Buildpack>,
        layer_path: &Path,
    ) -> anyhow::Result<LayerResult<Self::Metadata>> {

        let mut user_manifest = NamedTempFile::new()?;
        let mut user_manifest_toml = fs::read_to_string(context.app_dir.join("app.manifest"))?;
        let launch_envs = read_launch_envs(&self.gramine_layer_path)?;
        for (key, value) in launch_envs.iter() {
            user_manifest_toml.push_str(&format!("\nloader.env.{} = \"{}\"", key.to_str().unwrap(), value.to_str().unwrap()));
        }
        user_manifest.write_all(user_manifest_toml.as_bytes())?;

        let signing_key_path = save_signing_key()?;
        let mut processes: Vec<Process> = vec![];

        for process in read_launch_processes()? {
            processes.push(sign(
                process,
                &context.buildpack_dir,
                user_manifest.path(),
                layer_path,
                signing_key_path.path(),
                &self.gramine_layer_path,
                &self.gramine_env,
                &launch_envs,
                false,
            )?);
        }

        let process = Process::new(
            process_type!("bash"),
            "/bin/bash",
            None::<Vec<String>>,
            None,
            None,
        );

        processes.push(sign(
            process,
            &context.buildpack_dir,
            user_manifest.path(),
            layer_path,
            signing_key_path.path(),
            &self.gramine_layer_path,
            &self.gramine_env,
            &launch_envs,
            true,
        )?);

        LayerResultBuilder::new(SignLayerMetadata { processes }).build()
    }
}

fn read_launch_processes() -> anyhow::Result<Vec<Process>> {
    let mut processes: HashMap<String, Process> = HashMap::new();

    let mut entries: Vec<_> = glob("/layers/*/launch.toml").expect("Failed to read glob pattern").collect();

    // sort by modified timestamp
    entries.sort_by_key(|a| fs::metadata(a.as_ref().unwrap()).unwrap().modified().unwrap());

    for entry in entries {
        let toml_string = fs::read_to_string(&entry?)?;
        if let Ok::<Launch, _>(config) = toml::from_str(&toml_string) {
            for process in config.processes {
                let ptype = process.r#type.to_string();
                match processes.get_mut(&ptype) {
                    Some(old_process) => old_process.update(process),
                    None => { processes.insert(ptype,process); },
                }
            }
        }
    }

    Ok(processes.into_values().collect())
}

fn read_launch_envs(gramine_layer_path: &str) -> anyhow::Result<Env> {
    let mut env = Env::new();

    for entry in glob("/layers/*/*/").expect("Failed to read glob pattern") {
        let path = &entry?;
        if let Ok(layer_env) = LayerEnv::read_from_layer_dir(path) {
            env = layer_env.apply(TargetLifecycle::Launch, &env);
        }
    }
    env = LayerEnv::new()
        .chainable_insert(
            TargetLifecycle::All,
            ModificationBehavior::Prepend,
            "LD_LIBRARY_PATH",
            gramine_layer_path.to_string()+"/usr/lib/x86_64-linux-gnu/gramine/runtime/glibc",
        )
        .chainable_insert(
            TargetLifecycle::All,
            ModificationBehavior::Append,
            "LD_LIBRARY_PATH",
            "/usr/local/lib:/lib/x86_64-linux-gnu:/usr/lib/x86_64-linux-gnu:/lib:/usr/lib",
        )
        .chainable_insert(
            TargetLifecycle::All,
            ModificationBehavior::Delimiter,
            "LD_LIBRARY_PATH",
            ":",
        )
        .chainable_insert(
            TargetLifecycle::All,
            ModificationBehavior::Append,
            "PATH",
            "/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin:/bin:/sbin",
        )
        .chainable_insert(
            TargetLifecycle::All,
            ModificationBehavior::Delimiter,
            "PATH",
            ":",
        )
        .apply(TargetLifecycle::Launch, &env);


    Ok(env)
}

fn save_signing_key() -> anyhow::Result<NamedTempFile> {
    let signing_key_file = NamedTempFile::new()?;

    OpenOptions::new()
        .write(true)
        .create(true)
        .open(signing_key_file.path())?
        .write_all(&base64::decode(env::var("BP_SGX_SIGNING_KEY")?)?)?;

    Ok(signing_key_file)
}

fn is_binary(path: &Path) -> anyhow::Result<bool> {
    let bin_data = fs::read(path)?;
    Ok(object::File::parse(&*bin_data).is_ok())
}

fn sign(
    process: Process,
    buildpack_dir: &Path,
    user_manifest_path: &Path,
    layer_path: &Path, 
    signing_key_path: &Path,
    gramine_layer_path: &str, 
    gramine_env: &Env, 
    launch_env: &Env, 
    insecure_args: bool,
) -> anyhow::Result<Process> {
    let parameter_expansion = true;
    let process_type = process.r#type.as_str();
    let mut argv = process.command.split_whitespace().map(|s| s.to_string()).collect::<Vec<String>>();
    if let Some(mut args) = process.args {
        argv.append(&mut args);
    }

    let command_path = which::which_in(&argv[0], launch_env.get("PATH"), "/").unwrap();
    if is_binary(&command_path)? && !parameter_expansion {
        argv[0] = command_path.display().to_string();
    } else {
        argv = vec![String::from("/bin/bash"), String::from("-c"), argv.join(" ")];
    }

    let process_path = &layer_path.join(process_type);
    fs::create_dir_all(process_path)?;

    let finalize_manifest_path = buildpack_dir.join("finalize_manifest.py");
    let manifest_template_path = buildpack_dir.join("entrypoint.manifest.template");
    let apploader_template_path = buildpack_dir.join("apploader.template");
    let trusted_argv_path = &process_path.join("entrypoint.trusted_argv");
    let manifest_path = &process_path.join("entrypoint.manifest");
    let output_path = &process_path.join("entrypoint.manifest.sgx");
    let signature_path = &process_path.join("entrypoint.sig");
    let apploader_path = &process_path.join("apploader");

    fs::copy(apploader_template_path, apploader_path)?;

    if !insecure_args {
        println!("---> Serializing gramine arguments");
        let argv_serializer_result = Command::new("gramine-argv-serializer")
            .args(&argv)
            .envs(gramine_env)
            .output()?;

        if !argv_serializer_result.status.success() {
            return Err(anyhow::anyhow!("Failed to serialize gramine arguments"));
        }

        let argv_serialized = str::from_utf8(&argv_serializer_result.stdout)?;

        let mut file = OpenOptions::new()
            .write(true)
            .create(true)
            .open(trusted_argv_path)?;
        file.write_all(argv_serialized.as_bytes())?;
    }

    println!("---> Generating manifest");

    let finalize_mainfest_exit_code = Command::new("python")
        .args(&[
            "-B",
            finalize_manifest_path.to_str().unwrap(),
            "--template", manifest_template_path.to_str().unwrap(),
            "--argv", trusted_argv_path.to_str().unwrap(),
            "--entrypoint", &argv[0],
            "--gramine", gramine_layer_path,
            "--out", manifest_path.to_str().unwrap(),
            "--user", user_manifest_path.to_str().unwrap(),
            "--dir", "/workspace"
        ])
        .envs(gramine_env)
        .current_dir(process_path)
        .spawn()?
        .wait()?;

    if !finalize_mainfest_exit_code.success() {
        return Err(anyhow::anyhow!("Could not generate manifest"));
    }

    println!("---> Signing manifest");

    let sign_exit_code = Command::new("gramine-sgx-sign")
        .args(&[
            "--key", signing_key_path.to_str().unwrap(),
            "--manifest", manifest_path.to_str().unwrap(),
            "--output", output_path.to_str().unwrap(),
            "--sigfile", signature_path.to_str().unwrap(),
        ])
        .envs(gramine_env)
        .current_dir(process_path)
        .spawn()?
        .wait()?;

    if !sign_exit_code.success() {
        return Err(anyhow::anyhow!("Could not sign manifest"));
    }

    Ok(Process {
        r#type: format!("{}-gramine", process_type).parse().unwrap(),
        command: apploader_path.display().to_string(),
        args: None,
        direct: None,
        default: process.default,
    })
}
