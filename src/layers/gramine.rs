use sha2::Digest;
use std::fs;
use std::fs::OpenOptions;
use std::io;
use std::io::Write;
use std::path::Path;

use debarchive::Archive;
use fs_extra::dir::{copy, CopyOptions};
use tempfile::NamedTempFile;

use crate::GramineBuildpack;
use libcnb::build::BuildContext;
use libcnb::data::layer_content_metadata::LayerTypes;
use libcnb::generic::GenericMetadata;
use libcnb::layer::{Layer, LayerResult, LayerResultBuilder};
use libcnb::layer_env::{LayerEnv, ModificationBehavior, TargetLifecycle};

pub struct GramineLayer;

impl Layer for GramineLayer {
    type Buildpack = GramineBuildpack;
    type Metadata = GenericMetadata;

    fn types(&self) -> LayerTypes {
        LayerTypes {
            build: true,
            launch: true,
            cache: true,
        }
    }


    fn create(
        &self,
        context: &BuildContext<Self::Buildpack>,
        layer_path: &Path,
    ) -> anyhow::Result<LayerResult<Self::Metadata>> {
        println!("---> Download and extracting Gramine and dependencies");

        download_verify_unpack(
            &context.buildpack_descriptor.metadata.libprotobuf_url,
            &context.buildpack_descriptor.metadata.libprotobuf_checksum,
            &layer_path
        )?;

        if context.buildpack_dir.join("gramine").exists() {
            copy(context.buildpack_dir.join("gramine/bin"), layer_path.join("usr"), &CopyOptions::new())?;
            copy(context.buildpack_dir.join("gramine/lib"), layer_path.join("usr"), &CopyOptions::new())?;
        } else {
            download_verify_unpack(
                &context.buildpack_descriptor.metadata.gramine_url,
                &context.buildpack_descriptor.metadata.gramine_checksum,
                &layer_path
            )?;
        }

        println!("---> patching Gramine");
        let files_to_patch = vec![
            "usr/bin/gramine-direct",
            "usr/bin/gramine-sgx",
            "usr/lib/python3/dist-packages/graminelibos/__init__.py",
        ];

        for file_to_patch in files_to_patch {
            let path = layer_path.join(file_to_patch);
            let str_orig = fs::read_to_string(&path)?;
            let str_patched = str_orig
                .replace("/usr/lib/x86_64-linux-gnu", &format!("{}/usr/lib/x86_64-linux-gnu", layer_path.display()));

            let mut file = OpenOptions::new()
                .write(true)
                .truncate(true)
                .open(path)?;
            file.write_all(str_patched.as_bytes())?;
        }

        LayerResultBuilder::new(GenericMetadata::default())
            .env(
                LayerEnv::new()
                    .chainable_insert(
                        TargetLifecycle::All,
                        ModificationBehavior::Prepend,
                        "PATH",
                        layer_path.join("usr/bin"),
                    )
                    .chainable_insert(
                        TargetLifecycle::All,
                        ModificationBehavior::Delimiter,
                        "PATH",
                        ":",
                    )
                    .chainable_insert(
                        TargetLifecycle::All,
                        ModificationBehavior::Prepend,
                        "LD_LIBRARY_PATH",
                        layer_path.join("usr/lib/x86_64-linux-gnu"),
                    )
                    .chainable_insert(
                        TargetLifecycle::All,
                        ModificationBehavior::Delimiter,
                        "LD_LIBRARY_PATH",
                        ":",
                    )
                    .chainable_insert(
                        TargetLifecycle::All,
                        ModificationBehavior::Prepend,
                        "PYTHONPATH",
                        layer_path.join("usr/lib/python3/dist-packages/"),
                    )
                    .chainable_insert(
                        TargetLifecycle::All,
                        ModificationBehavior::Delimiter,
                        "PYTHONPATH",
                        ":",
                    )
            )
            .build()
    }
}

fn download_verify_unpack(uri: impl AsRef<str>, checksum: &str, layer_path: impl AsRef<Path>) -> anyhow::Result<()> {

    let tmp_deb = NamedTempFile::new()?;

    download(uri, tmp_deb.path())?;
    assert_eq!(&sha256_checksum(tmp_deb.path())?,checksum);

    undeb(tmp_deb.path(), layer_path)?;

    Ok(())
}

fn download(uri: impl AsRef<str>, dst: impl AsRef<Path>) -> anyhow::Result<()> {
    let response = ureq::get(uri.as_ref()).call()?;
    let mut reader = response.into_reader();
    let mut file = fs::File::create(dst.as_ref())?;
    io::copy(&mut reader, &mut file)?;

    Ok(())
}

fn undeb(file: impl AsRef<Path>, dst: impl AsRef<Path>) -> anyhow::Result<()> {
    let archive = Archive::new(file.as_ref()).unwrap();
    archive.data_extract(dst.as_ref())?;

    Ok(())
}

fn sha256_checksum(path: impl AsRef<Path>) -> anyhow::Result<String> {
    Ok(fs::read(path)
        .map(|bytes| sha2::Sha256::digest(bytes.as_ref()))
        .map(|bytes| format!("{:x}", bytes))?)
}
