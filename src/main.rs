use crate::layers::{GramineLayer, PipLayer, SignLayer};
use libcnb::build::{BuildContext, BuildResult, BuildResultBuilder};
use libcnb::data::build_plan::{BuildPlan, Require};
use libcnb::data::launch::{Launch};
use libcnb::data::layer_name;
use libcnb::detect::{DetectContext, DetectResult, DetectResultBuilder};
use libcnb::generic::GenericPlatform;
use libcnb::layer_env::TargetLifecycle;
use libcnb::{buildpack_main, Buildpack, Env};

use glob::glob;
use serde::Deserialize;
use std::env;
use std::fs;
use std::path::{Path, PathBuf};
use toml::map::Map;
use x509_parser::pem::Pem;

mod layers;

#[derive(Deserialize, Debug)]
pub struct GramineBuildpackMetadata {
    pub gramine_url: String,
    pub gramine_checksum: String,
    pub libprotobuf_url: String,
    pub libprotobuf_checksum: String,
}

pub struct GramineBuildpack;

impl Buildpack for GramineBuildpack {
    type Platform = GenericPlatform;
    type Metadata = GramineBuildpackMetadata;
    type Error = anyhow::Error;

    fn detect(&self, context: DetectContext<Self>) -> libcnb::Result<DetectResult, Self::Error> {
        if context.app_dir.join("app.manifest").exists() {
            if !check_signing_key().is_ok() {
                eprintln!("Failure to read signing key, please provide your signing key file as \
                          base64 string via the $BP_SGX_SIGNING_KEY env variable, e.g. \
                          'BP_SGX_SIGNING_KEY=$(base64 -w0 path/to/your/signing-key.pem)'");
                return DetectResultBuilder::fail().build();
            }
            fs::copy(context.app_dir.join("app.manifest"), "/tmp/app.manifest").unwrap();

            let mut build_plan = BuildPlan::new();
            build_plan.requires = vec![
                Require { name: String::from("pip"), metadata: 
                    Map::from_iter([(
                        String::from("build"), toml::Value::Boolean(true))
                    ])
                },
                Require { name: String::from("cpython"), metadata: 
                    Map::from_iter([
                        (String::from("build"), toml::Value::Boolean(true)),
                        (String::from("launch"), toml::Value::Boolean(true))
                    ])
                }
            ];
            DetectResultBuilder::pass()
                .build_plan(build_plan)
                .build()
        } else {
            DetectResultBuilder::fail().build()
        }
    }

    fn build(&self, context: BuildContext<Self>) -> libcnb::Result<BuildResult, Self::Error> {
        println!("---> Gramine Buildpack");

        if let Some(folders) = detect_profiled_execd() {
            eprintln!("WARNING: buildpacks were detected which modify the state of the container during launch via \
                profile.d/exec.d scripts. These modifications are unknown to the gramine manifest and are likely to \
                cause trouble during the execution of the enclave.\nList of detected scripts:\n{:#?}", folders);
        }

        match env::var("BP_GRAMINE_SLEEP") {
            Ok(_) => std::thread::sleep(std::time::Duration::from_secs(60*60)),
            Err(_) => {},
        };

        if !context.app_dir.join("app.manifest").exists() {
            if !Path::new("/tmp/app.manifest").exists() {
                return Err(anyhow::anyhow!("app.manifest was deleted from workspace by a previous buildpack
and could not be recovered from cache.").into());
            }
            fs::copy("/tmp/app.manifest", context.app_dir.join("app.manifest")).unwrap();
        }

        let gramine_layer = context.handle_layer(layer_name!("gramine"), GramineLayer)?;

        let pip_layer = context.handle_layer(layer_name!("pip"), PipLayer)?;

        let gramine_env = pip_layer.env.apply(TargetLifecycle::Build, 
            &gramine_layer.env.apply(TargetLifecycle::Build, &Env::from(env::vars_os())));

        let sign_layer = context.handle_layer(
            layer_name!("sign"),
            SignLayer {
                gramine_layer_path: gramine_layer.path.into_os_string().into_string().unwrap(),
                gramine_env,
            },
        )?;

        let mut launch = Launch::new();

        for process in sign_layer.content_metadata.metadata.processes {
            launch = launch.process(process);
        }

        BuildResultBuilder::new().launch(launch).build()
    }
}

fn check_signing_key() -> anyhow::Result<()> {
    let sgx_signing_key_str = base64::decode(env::var("BP_SGX_SIGNING_KEY")?)?;

    for pem in Pem::iter_from_buffer(&sgx_signing_key_str) {
        pem?;//.parse_x509()?;
        return Ok(());
    }

    Err(anyhow::anyhow!(""))
}

fn detect_profiled_execd() -> Option<Vec<PathBuf>> {
    let folders: Vec<PathBuf> = glob("/layers/*/*/profile.d").unwrap().filter_map(Result::ok)
                .chain(glob("/layers/*/*/exec.d").unwrap().filter_map(Result::ok)).collect();

    if folders.is_empty() {
        return None;
    }
    Some(folders)
}

buildpack_main!(GramineBuildpack);
