#!/usr/bin/env python3
# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright (C) 2020-2021 Intel Corp.
#                         Anjo Vahldiek-Oberwagner <anjo.lucas.vahldiek-oberwagner@intel.com>
#                         Dmitrii Kuvaiskii <dmitrii.kuvaiskii@intel.com>

import argparse
import os
import re
import subprocess
import sys
import glob

import jinja2
import toml

def is_utf8(filename_bytes):
    try:
        filename_bytes.decode('UTF-8')
        return True
    except UnicodeError:
        return False

def extract_files_from_user_manifest(manifest):
    files = []

    files.extend(manifest['sgx'].get('trusted_files', []))
    files.extend(manifest['sgx'].get('allowed_files', []))
    files.extend(manifest['sgx'].get('protected_files',[]))

    return files

def generate_trusted_files_layers(already_added_files):
    listing = glob.glob('/layers/*/*.toml')

    trusted_files = []
    for file in listing:
        config = toml.load(file)
        add = False
        try:
            add = config['types']['launch']
        except KeyError:
            pass
        try:
            add = config['launch']
        except KeyError:
            pass

        if add:
            root_dir = file.split(".toml")[0]
            trusted_files.extend(generate_trusted_files_direct(root_dir, already_added_files, None))

    return trusted_files

def generate_trusted_files(root_dir, already_added_files):
    excluded_paths_regex = (r'^/('
                                r'boot/.*'
                                r'|\.dockerenv'
                                r'|\.dockerinit'
                                r'|etc/mtab'
                                r'|dev/.*'
                                r'|etc/rc(\d|.)\.d/.*'
                                r'|gramine/python/.*'
                                r'|layers/.*'
                                r'|proc/.*'
                                r'|sys/.*'
                                r'|var/.*)$')
    exclude_re = re.compile(excluded_paths_regex)

    return generate_trusted_files_direct(root_dir, already_added_files, exclude_re)

def generate_trusted_files_direct(root_dir, already_added_files, exclude_re):

    num_trusted = 0
    trusted_files = []
    for root, _, files in os.walk(root_dir.encode('UTF-8'), followlinks=False):
        for file in files:
            filename = os.path.join(root, file)
            if not os.path.isfile(filename):
                # only regular files are added as trusted files
                continue
            if not os.access(filename, os.R_OK):
                # Skip inaccessible files
                continue
            if not is_utf8(filename):
                # we append filenames as TOML strings which must be in UTF-8
                print(f'\t[Finalize manifest] File {filename} is not in UTF-8!')
                sys.exit(1)

            # convert from bytes to str for further string handling
            filename = filename.decode('UTF-8')

            if exclude_re != None and exclude_re.match(filename):
                # exclude special files and paths from list of trusted files
                continue
            if '\n' in filename:
                # we use TOML's basic single-line strings, can't have newlines
                continue

            trusted_file_entry = f'file:{filename}'
            if trusted_file_entry in already_added_files:
                # user manifest already contains this file (probably as allowed or protected)
                continue

            trusted_files.append(trusted_file_entry)
            num_trusted += 1

    print(f'\t[Finalize manifest] Found {num_trusted} files in `{root_dir}`.')
    return trusted_files


def generate_library_paths():
    encoding = sys.stdout.encoding if sys.stdout.encoding is not None else 'UTF-8'
    ld_paths = subprocess.check_output('ldconfig -v -C /tmp/ld.so.cache~', stderr=subprocess.PIPE, shell=True)
    ld_paths = ld_paths.decode(encoding).splitlines()

    # Library paths start without whitespace (in contrast to libraries found under this path)
    ld_paths = (line for line in ld_paths if not re.match(r'(^\s)', line))
    return ''.join(ld_paths) + os.getenv('LD_LIBRARY_PATH', default='')


argparser = argparse.ArgumentParser()
argparser.add_argument('-d', '--dir', default='/',
    help='Search directory tree from this root to generate list of trusted files.')
argparser.add_argument('-o', '--out', default='entrypoint.manifest',
    help='File to write final manifest to.')
argparser.add_argument('-u', '--user', default='app.manifest',
    help='User supplied manifest file.')
argparser.add_argument('-t', '--template', default='entrypoint.manifest.template',
    help='Manifest template file.')
argparser.add_argument('-g', '--gramine',
    help='Path to gramine layer.')
argparser.add_argument('-e', '--entrypoint',
    help='Path to entrypoint.')
argparser.add_argument('-a', '--argv',
    help='Trusted argv.')

def merge_manifest(entrypoint_manifest_dict, user_manifest_path):
    #env = jinja2.Environment(loader=jinja2.FileSystemLoader('./'))
    #env.globals.update(yaml.safe_load(args.config_file))
    #env.globals.update(vars(args))

    # generate entrypoint.manifest from three parts:
    #   - Jinja-style templates/entrypoint.manifest.template
    #   - base Docker image's environment variables
    #   - additional, user-provided manifest options
    #entrypoint_manifest_render = env.get_template('entrypoint.manifest.template').render()
    #entrypoint_manifest_dict = toml.loads(entrypoint_manifest_render)

    ## TODO environment vars
    #base_image_environment = extract_environment_from_image_config(original_image.attrs['Config'])
    #base_image_dict = toml.loads(base_image_environment)
    #entrypoint_manifest_dict = merge_two_dicts(entrypoint_manifest_dict, base_image_dict)

    user_manifest_contents = ''
    if os.path.exists(user_manifest_path):
        with open(user_manifest_path, 'r') as user_manifest_file:
            user_manifest_contents = user_manifest_file.read()
    user_manifest_dict = toml.loads(user_manifest_contents)

    # Support deprecated syntax: replace old-style TOML-dict (`sgx.trusted_files.key = "file:foo"`)
    # with new-style TOML-array (`sgx.trusted_files = ["file:foo"]`) in the user manifest
    if 'sgx' in user_manifest_dict:
        if 'trusted_files' in user_manifest_dict['sgx']:
            if isinstance(user_manifest_dict['sgx']['trusted_files'], dict):
                tf_list = list(user_manifest_dict['sgx']['trusted_files'].values())
                user_manifest_dict['sgx']['trusted_files'] = tf_list
        if 'allowed_files' in user_manifest_dict['sgx']:
            if isinstance(user_manifest_dict['sgx']['allowed_files'], dict):
                af_list = list(user_manifest_dict['sgx']['allowed_files'].values())
                user_manifest_dict['sgx']['allowed_files'] = af_list
        if 'protected_files' in user_manifest_dict['sgx']:
            if isinstance(user_manifest_dict['sgx']['protected_files'], dict):
                pf_list = list(user_manifest_dict['sgx']['protected_files'].values())
                user_manifest_dict['sgx']['protected_files'] = pf_list

    merged_manifest_dict = merge_two_dicts(user_manifest_dict, entrypoint_manifest_dict)

    return merged_manifest_dict

def merge_two_dicts(dict1, dict2, path=[]):
    for key in dict2:
        if key in dict1:
            if isinstance(dict1[key], dict) and isinstance(dict2[key], dict):
                merge_two_dicts(dict1[key], dict2[key], path + [str(key)])
            elif isinstance(dict1[key], list) and isinstance(dict2[key], list):
                dict1[key].extend(dict2[key])
            elif dict1[key] == dict2[key]:
                pass
            else:
                raise Exception(f'''Duplicate key with different values found: `{".".join(path +
                                   [str(key)])}`''')
        else:
            dict1[key] = dict2[key]
    return dict1

def main(args=None):
    args = argparser.parse_args(args[1:])
    if not os.path.isdir(args.dir):
        argparser.error(f'\t[Finalize manifest] Could not find directory `{args.dir}`.')

    env = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.dirname(args.template)))
    env.globals.update({
        'library_paths': generate_library_paths(), 
        'env_path': os.getenv('PATH'),
        'gramine_layer': args.gramine,
        'entrypoint': args.entrypoint,
        'trusted_argv': args.argv,
        'insecure_args': not os.path.isfile(args.argv),
    })

    manifest = os.path.basename(args.template)
    final_manifest = args.out
    rendered_manifest = env.get_template(manifest).render()
    rendered_manifest_dict = toml.loads(rendered_manifest)
    if os.path.isfile(args.user):
        rendered_manifest_dict = merge_manifest(rendered_manifest_dict, args.user)
    
    already_added_files = extract_files_from_user_manifest(rendered_manifest_dict)
    trusted_files = generate_trusted_files('/workspace', already_added_files)
    trusted_files.extend(generate_trusted_files_layers(already_added_files))
    trusted_files.extend(generate_trusted_files(args.dir, already_added_files))
    trusted_files = list(dict.fromkeys(trusted_files))
    rendered_manifest_dict['sgx'].setdefault('trusted_files', []).extend(trusted_files)
    with open(final_manifest, 'w') as manifest_file:
        toml.dump(sortedDeep(rendered_manifest_dict), manifest_file)
    print(f'\t[Finalize manifest] Successfully finalized `{final_manifest}`.')

def sortedDeep(d):
    def makeTuple(v): return (*v,) if isinstance(v,(list,dict)) else (v,)
    if isinstance(d,list):
        return sorted( map(sortedDeep,d) ,key=makeTuple )
    if isinstance(d,dict):
        return { k: sortedDeep(d[k]) for k in sorted(d)}
        #return dict(sorted({ k: sortedDeep(d[k]) for k in sorted(d)}.items(), key=lambda item: item[1]))
    return d

if __name__ == '__main__':
    main(sys.argv)
